import { MatSnackBarConfig } from '@angular/material';

export const snackConfig: MatSnackBarConfig = {
    duration: 5000,
    horizontalPosition: 'right',
    verticalPosition: 'bottom'
};
