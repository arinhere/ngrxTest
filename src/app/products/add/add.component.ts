import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProductService } from '../../core/services/products.service';
import { MatSnackBar } from '@angular/material';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { snackConfig } from '../../customConfig';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})

export class AddProductComponent implements OnInit {

  // tslint:disable-next-line:variable-name
  constructor(private fb: FormBuilder, private _product: ProductService, private _snack: MatSnackBar,
    // tslint:disable-next-line:variable-name
    private _router: Router, private _route: ActivatedRoute) { }

  // Get the form controls for validation
  get f() { return this.addProductForm.controls; }
  pid = '';
  addProductForm: FormGroup;

  chars = 10;

  priceError = 0;

  ngOnInit() {
    this.addProductForm = this.fb.group({
      title: ['', [Validators.required, Validators.maxLength(10)]],
      description: [''],
      price: ['', [Validators.required, Validators.maxLength(4)]]
    });

    // Check if there is any ID in the param, then it will for edit purpose
    this._route.params.subscribe((params: Params) => {
      if (params['id']) {
        this.pid = params['id'];  // Getting data

        // Getting data from API & assign them
        this._product.getProductByID(this.pid)
          .subscribe(result => {
            this.addProductForm.get('title').setValue(result['body']['title']);
            this.addProductForm.get('description').setValue(result['body']['description']);
            this.addProductForm.get('price').setValue(result['body']['price']);
          });

      }
    });
  }

  onSubmit(formVal) {
    if (!this.pid) {
      this._product.addProduct(formVal.value)
        .subscribe(result => {
          this._snack.open('Product Added', 'Success', snackConfig);

          setTimeout(() => {
            this._router.navigate(['/products/list']);
          }, 1000);
        },
          err => {
            this._snack.open('Unable to add product', 'Error', snackConfig);
          });
    } else {
      const body = formVal.value;
      body._id = this.pid;

      this._product.updateProduct(body)
        .subscribe(result => {
          this._snack.open('Product Updated', 'Success', snackConfig);

          setTimeout(() => {
            this._router.navigate(['/products/list']);
          }, 1000);
        },
          err => {
            this._snack.open('Unable to update product', 'Error', snackConfig);
          });
    }

  }
  checkVal(ev, oldStr) {
    if (ev.keyCode === 109) {
      this.addProductForm.controls.price.setValue('');
      this.priceError = 1;
    } else {
      this.priceError = 0;
    }
  }

}
