import { Router, CanActivate, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { snackConfig } from '../../customConfig';

@Injectable()
export class AuthGuard implements CanActivate {

    // tslint:disable-next-line:variable-name
    constructor(private _router: Router, private _snackBar: MatSnackBar) { }

    // Prevent unauthorized access to dashboard module
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        // If user role needs to be checked
        // const role = route.data.role as Array<string>;

        if (localStorage.getItem('token')) {
            return true;
        }

        this._snackBar.open('Unauthorized. Login to continue', 'Error', snackConfig);
        this._router.navigate(['/']);
        return false;

    }
}
